package edu.sjsu.ngfarmer.tester;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.sjsu.ngfarmer.driver.*;

public class S3ServiceTester {

	private static S3Service s3 = null;

	 @Before
	    public void setUp(){
		 	s3 = new S3Service();
	    }

	    @Test
	    public void testIsBucketExist() {
	    	String bucketName = "ngfarmer";
	    	String key = "result";
	    	try{
	    		boolean result = s3.isObjectExist(bucketName,key);
	    		assertTrue("Folder is exist under bucket "+bucketName,result == true);
	    	}
	    	catch(Exception e)
	    	{
	    		e.printStackTrace();
	    	}
	    }
	    @Test
	    public void testMoveAndDeleteObjectsInFolder() {
	    	String bucketName = "ngfarmer";
	    	String key = "result";
	    	try{
	    		//s3.MoveAndDeleteObjectsInFolder(bucketName,key);
	    		boolean result = s3.isObjectExist(bucketName,key);
	    		assertTrue("Folder deleted after backup.",result == true);
	    	}
	    	catch(Exception e)
	    	{
	    		e.printStackTrace();
	    	}
	    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}


}
