package edu.sjsu.ngfarmer.tester;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import edu.sjsu.ngfarmer.driver.*;

public class EMRContollerTester {

	private static EMRDriver emrDriver = null;

	 @Before
	    public void setUp(){
		 	emrDriver = new EMRDriver();
	    }

	    @Test
	    public void testInit() {
	    	int result=1;
	    	try{
	    		result = emrDriver.init();
		    	assertTrue("EMR Instance Created Successfully.",result == 1);
	    	}
	    	catch(Exception e)
	    	{
	    		e.printStackTrace();
	    	}
	    }
	    @Test
	    public void testSetBucketName() {
	    	String bucketName = "MyFarmerPortal";
	    	try{
	    		emrDriver.setBucketName(bucketName);
	    		String str = emrDriver.getBucketName();
	    		if (str !=null) {
	    			System.out.println("Bucket Name : " + str);
	    			assertEquals(str, bucketName);
	    		}		
	    	}
	    	catch(Exception e)
	    	{
	    		e.printStackTrace();
	    	}
	    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}


}
