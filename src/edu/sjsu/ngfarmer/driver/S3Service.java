package edu.sjsu.ngfarmer.driver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

/**
 * This Class is used to provide utility functions to move files to/from S3.
 * In addition, it interacts with RDS and DynamoDB to retrieve records and store in S3.
 *
 */
public class S3Service {
    private static AWSCredentials credentials;
    private static AmazonS3 s3;
    private static String currentDate;
    private static  String BUCKET_NAME = "ngfarmer"; //default

    public Map commodityMap;
    public Map marketMap;
    public static final int FARMERS_MARKET_DATA = 0;
    public static final int FARMERS_SALES_DATA  = 1;
    public static String FARMERS_MARKET_DATA_FILE_PREFIX="farmers-market-data";
    public static String FARMERS_SALES_DATA_FILE_PREFIX="farmers-sales-data";
    public static String FARMERS_DATA_FILE_SUFFIX=".txt";


    static AmazonDynamoDBClient dynamoDB;
    String tableName = "BestMarketPrice";
    
    public S3Service() {
        s3 = new AmazonS3Client(credentials = new ClasspathPropertiesFileCredentialsProvider().getCredentials());
		Region usWest2 = Region.getRegion(Regions.US_WEST_2);
		s3.setRegion(usWest2);   	
    }
    public S3Service(String bucketName) {
        s3 = new AmazonS3Client(credentials = new ClasspathPropertiesFileCredentialsProvider().getCredentials());
		Region usWest2 = Region.getRegion(Regions.US_WEST_2);
		s3.setRegion(usWest2);

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm");
		Calendar cal = Calendar.getInstance();
		currentDate = dateFormat.format(cal.getTime());
		
		dynamoDB = new AmazonDynamoDBClient(new ClasspathPropertiesFileCredentialsProvider());
	    dynamoDB.setRegion(usWest2);
		
	    BUCKET_NAME = bucketName;
	    try
	    {
	    	commodityMap = getCommodityIds();
	    	System.out.println("Commodity Map Size from RDS = "+commodityMap.size());
            //printMap(commodityMap);
	    	marketMap = getMarketIds();
	    	System.out.println("Market Map Size from RDS = "+marketMap.size());
            //printMap(marketMap);

	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
		
    }
    public void setBucketName(String bucketname) {
    	BUCKET_NAME = bucketname;
    }
    public String getBucketName() {
    	return BUCKET_NAME;
    }
	public void backupResult(String bucketName, String key) {
		
		try {
		    /*
	         * Moving to a new folder and Delete an object - 
	         * Unless versioning has been turned on for your bucket,
	         * there is no way to undelete an object, so use caution when deleting objects.
	         */
			MoveAndDeleteObjectsInFolder(bucketName, key);
			System.out.println("Deleting an object\n");
		} catch (AmazonServiceException ase) {
			System.out.println("Caught an AmazonServiceException, which means your request made it "
					+ "to Amazon S3, but was rejected with an error response for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			System.out.println("Caught an AmazonClientException, which means the client encountered "
					+ "a serious internal problem while trying to communicate with S3, "
					+ "such as not being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		}
  }
	/** 
	 * This method is used to move content of the result folder to a backup folder and delete the result
	 * The result directory must not exist for the EMR cluster. 
	 * EMR Cluster create the directory and store the results.
	 *  
	 * @param bucketName The bucket name where the folder is located.
	 * @param folderPath The folder path where the result files are located.
	 */
	public void MoveAndDeleteObjectsInFolder(String bucketName, String folderPath) {
		String destinationFolder = "backup-"+currentDate+"/";	
		
		System.out.println("Moving files from "+bucketName+"/"+folderPath + " to "+bucketName+"/"+destinationFolder);

		for (S3ObjectSummary file : s3.listObjects(bucketName, folderPath).getObjectSummaries()) {
			String S3Key = file.getKey();
			System.out.println("File retrieved  "+S3Key);
			String key = S3Key.substring(S3Key.indexOf('/') + 1);

			if (key != "") {			
				System.out.println("Copying file "+bucketName+"/"+S3Key);
				CopyObjectRequest copyObjRequest = new CopyObjectRequest(bucketName, file.getKey() , bucketName, destinationFolder+key);
				s3.copyObject(copyObjRequest);
			}
			System.out.println("Deleting file "+S3Key);
			s3.deleteObject(bucketName, file.getKey());
		}
	}
	public boolean isObjectExist(String bucketName, String key) {
		for (S3ObjectSummary file : s3.listObjects(bucketName, key).getObjectSummaries()) {
		String S3Key = file.getKey();
		String result = S3Key.substring(0,S3Key.indexOf('/'));
		System.out.println("[isObjectExist] File Name: "+S3Key + " "+result);
			if (result.equalsIgnoreCase(key)) {
				return true;
			}
		}
		return false;
	}
	public void transferFromS3toDynoDb(String bucketName, String key) throws IOException {
		S3Object object = s3.getObject(new GetObjectRequest(bucketName, key));
		System.out.println("Content-Type: "  + object.getObjectMetadata().getContentType());
		try {
			loadTextInputStream(object.getObjectContent());
		 } catch (AmazonServiceException ase) {
	            System.out.println("Caught an AmazonServiceException, which means your request made it "
	                    + "to Amazon S3, but was rejected with an error response for some reason.");
	            System.out.println("Error Message:    " + ase.getMessage());
	            System.out.println("HTTP Status Code: " + ase.getStatusCode());
	            System.out.println("AWS Error Code:   " + ase.getErrorCode());
	            System.out.println("Error Type:       " + ase.getErrorType());
	            System.out.println("Request ID:       " + ase.getRequestId());
	        } catch (AmazonClientException ace) {
	            System.out.println("Caught an AmazonClientException, which means the client encountered "
	                    + "a serious internal problem while trying to communicate with S3, "
	                    + "such as not being able to access the network.");
	            System.out.println("Error Message: " + ace.getMessage());
	        }
	}
    /**
     * Displays the contents of the specified input stream as text.
     *
     * @param input
     *            The input stream to display as text.
     *
     * @throws IOException
     */
    private void loadTextInputStream(InputStream input) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        while (true) {
            String line = reader.readLine();
            if (line == null) break;
            
            recordExport(line);
        }
        System.out.println();
    }
	/**
	 * This method is used to get all Market info from RDS
	 * @return A map with having market name and market id..
	 * @throws SQLException
	 */
	public Map getMarketIds() throws SQLException
	{
		Map<String,Long> markets = new HashMap<String,Long>();
		
		Connection connection = getRDSConnection();
		if(connection != null)
		{
			Statement stmt = connection.createStatement();
			ResultSet resultSet = stmt.executeQuery("SELECT * FROM fixedmarket ");
			while (resultSet.next()) 
			{
				
				long marketId = resultSet.getLong("MARKET_ID");
				String name = resultSet.getString("NAME");
				markets.put(name, marketId);
			}
		}
		closeConnecton(connection);
		return markets;
		
	}
	/**
	 * This method is used to get all commodity info from RDS
	 * @return A map with having commodity name and Id.
	 * @throws SQLException
	 */
	public Map getCommodityIds() throws SQLException
	{
		Map<String,Long> commodities = new HashMap<String,Long>();
		Connection connection = getRDSConnection();
		if(connection != null)
		{
			Statement stmt = connection.createStatement();
			ResultSet resultSet = stmt.executeQuery("SELECT * FROM commodity");
			while (resultSet.next()) 
			{
				
				long commodityId = resultSet.getLong("COMMODITY_ID");
				String name = resultSet.getString("NAME");
				
				commodities.put(name.trim(),commodityId);
			}
		}
		closeConnecton(connection);
		return commodities;
	}
	
	// You need to close the resultSet
	public void closeConnecton(Connection connection) {
		    try {
		      if (connection != null) {
		    	  connection.close();
		      }
		    } catch (Exception e) {
		    	e.printStackTrace();
		    }
		  }

	public Connection getRDSConnection()
	{
		Connection connection = null;
		try 
		{
			// This will load the MySQL driver, each DB has its own driver
			Class.forName("com.mysql.jdbc.Driver");
			// Setup the connection with the DB
			connection = DriverManager.getConnection("jdbc:mysql://aa2zoawdyvh9uy.crsn8f73vqli.us-west-2.rds.amazonaws.com:3306/ebdb?user=radukkad&password=passw0rd");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return connection;
	}
	/**
	 * This method is used to export best price records to DynamoDB
	 * @param line the record entry
	 * @return
	 */
	public boolean recordExport(String line)
	{
		long commKey = 0;
		long marketKey = 0;
		String[] recSplit = line.split(",");
		if(recSplit.length == 7)
		{
			String commName = recSplit[0];
            //System.out.println("[recordExport] commName="+commName);
            
            if (commodityMap.containsKey(commName)) {
    			commKey = (Long)commodityMap.get(commName.trim());
    			
            } else {
            	System.out.println("Invalid Commodity Name...Exiting..");
            	return false;
            }            
			String marketName=recSplit[1];
			if (marketMap.containsKey(marketName)) {
				marketKey = (Long)marketMap.get(marketName.trim());
			} else {
            	System.out.println("Invalid Market Name...Exiting..");
            	return false;		
			}
			String date = recSplit[4];
			String cellNumber = recSplit[5]; /* cell no */
			String price = recSplit[6];

			
			String col1Val = commKey +"_"+  marketKey;
			
			  // Add an item
            Map<String, AttributeValue> item = newItem(col1Val, date, cellNumber, price);
            PutItemRequest putItemRequest = new PutItemRequest(tableName, item);
            PutItemResult putItemResult = dynamoDB.putItem(putItemRequest);
            //System.out.println("Result: " + putItemResult);
            System.out.println(" Exporting line : "+col1Val+" *** "+ line);
            
		}	
		return true;

	}
	/**
	 * This method is used to transfer market and sales record from RDS to S3.
	 * @throws SQLException
	 * @throws IOException
	 */
	public void transferFromRDStoS3() throws SQLException, IOException
	{	
		List marketDataRecords = new ArrayList();
		Connection connection = getRDSConnection();
		if(connection != null)
		{
			Statement stmt = connection.createStatement();
			StringBuffer queryStrMarket = new StringBuffer("select comm.name as commodityname, fm.name as marketname,fm.district as district,fm.state as state,sale_date as saledate ,sale_price as saleprice from marketdata md ");
			queryStrMarket.append(" inner join commodity comm  on comm.COMMODITY_ID = md.commodity_id");
			queryStrMarket.append(" inner join fixedmarket fm on  fm.market_id = md.market_id");

			ResultSet resultSet = stmt.executeQuery(queryStrMarket.toString());
			while (resultSet.next()) 
			{
				String marketName = resultSet.getString("marketname");
				String commodityName = resultSet.getString("commodityname");
				String district = resultSet.getString("district");
				String state = resultSet.getString("state");
				String saleDate = resultSet.getString("saledate");
				double salePrice = resultSet.getDouble("saleprice");
				String cellNumber = "0";
				String recString = cellNumber+","+commodityName +","+ marketName + "," + district +"," + state +"," + saleDate + "," + salePrice;
				marketDataRecords.add(recString);
			}
			
			storeMarketData(marketDataRecords);
			resultSet.close();
			
			List salesDataRecords = new ArrayList();
			
			StringBuffer queryStrComm = new StringBuffer("select comm.name as commodityname,fm.name as marketname,fm.district as district,fm.state as state,sale_date as saledate ,sale_price as saleprice, fmr.cellnumber as fmrId from salesdata sd ");
			queryStrComm.append("inner join commodity comm  on comm.COMMODITY_ID = sd.commodity_id inner join fixedmarket fm on  fm.market_id = sd.market_id ");
			queryStrComm.append("inner join farmer fmr on fmr.farmer_id = sd.farmer_id;");
			resultSet = stmt.executeQuery(queryStrComm.toString());
			while (resultSet.next()) 
			{
				String commodityName = resultSet.getString("commodityname");
				String marketName = resultSet.getString("marketname");
				String district = resultSet.getString("district");
				String state = resultSet.getString("state");
				String saleDate = resultSet.getString("saledate");
				double salePrice = resultSet.getDouble("saleprice");
				String cellNumber = resultSet.getString("fmrId");
	
				String recString = cellNumber+","+commodityName +","+ marketName + "," + district +"," + state +"," + saleDate + "," + salePrice;
				
				salesDataRecords.add(recString);				
			}
			storeSalesData(salesDataRecords);
		}
		closeConnecton(connection);
		
	}
	/**
	 * This method is used to store the market data imported from RDS to S3.
	 * @param marketDataRecords
	 * @throws IOException
	 */
	public void storeMarketData(List marketDataRecords) throws IOException {
		 String key="input/"+FARMERS_MARKET_DATA_FILE_PREFIX+FARMERS_DATA_FILE_SUFFIX;
		 s3.putObject(new PutObjectRequest(getBucketName(), key, 
		 			createFileForS3 (FARMERS_MARKET_DATA, marketDataRecords)));
		 System.out.println("Uploading Market data file to S3.\n");
	}
	/**
	 * This method is used to store the sales data imported from RDS to S3.
	 * @param salesDataRecords
	 * @throws IOException
	 */
	public void storeSalesData(List salesDataRecords) throws IOException {
		 String key="input/"+FARMERS_SALES_DATA_FILE_PREFIX + FARMERS_DATA_FILE_SUFFIX;
         s3.putObject(new PutObjectRequest(getBucketName(), key, 
        		 			createFileForS3 (FARMERS_SALES_DATA, salesDataRecords)));
		 System.out.println("Uploading Sales data file to S3.\n");
	}
	private static Map<String, AttributeValue> newItem(String key, String date, String cellNumber, String price) {
		Map<String, AttributeValue> item = new HashMap<String, AttributeValue>();
	    item.put("CommodityID_MarketID", new AttributeValue(key));
	    item.put("Date", new AttributeValue(date));
	    item.put("CellNumber", new AttributeValue(cellNumber));
	    item.put("Price", new AttributeValue(price));
	    
	    return item;
	}
	/**
     * Creates market/sales data file with text data to upload
     * to Amazon S3
     *
     * @return A newly created file with text data.
     *
     * @throws IOException
     */
    private static File createFileForS3(int arg, List records) throws IOException {
    	File file=null;
    	if (arg == FARMERS_MARKET_DATA) {
    		file = File.createTempFile(FARMERS_MARKET_DATA_FILE_PREFIX, FARMERS_DATA_FILE_SUFFIX);
    	} else {
    		file = File.createTempFile(FARMERS_SALES_DATA_FILE_PREFIX, FARMERS_DATA_FILE_SUFFIX);
    	}

        Writer writer = new OutputStreamWriter(new FileOutputStream(file));
        for (int i=0; i < records.size(); i++) {
			String line = (String) records.get(i);
			writer.write(line+"\r");
			System.out.println(((arg == FARMERS_MARKET_DATA)?"["+i+".MarketData] ":"["+i+".SalesData] ")+line);
		}
        writer.close();
        return file;
    }
	public static void printMap(Map mp) {
	    Iterator it = mp.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pairs = (Map.Entry)it.next();
	        System.out.println(pairs.getKey() + " = " + pairs.getValue());
	    }
	}
	public static void main(String[] args) throws IOException {
		/* bucket name changes per aws a/c */
		String bucketName = "ngfarmer"; 
		String key = "result";

		S3Service svc = new S3Service();
		//svc.backupResult(bucketName, key);
		svc.transferFromS3toDynoDb(bucketName, key+"/part-r-00000");
		// once the import is done file must be deleted
	}
}