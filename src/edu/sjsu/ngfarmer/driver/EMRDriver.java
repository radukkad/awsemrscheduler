package edu.sjsu.ngfarmer.driver;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.model.InstanceType;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduce;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClient;
import com.amazonaws.services.elasticmapreduce.model.DescribeJobFlowsRequest;
import com.amazonaws.services.elasticmapreduce.model.DescribeJobFlowsResult;
import com.amazonaws.services.elasticmapreduce.model.HadoopJarStepConfig;
import com.amazonaws.services.elasticmapreduce.model.JobFlowDetail;
import com.amazonaws.services.elasticmapreduce.model.JobFlowExecutionState;
import com.amazonaws.services.elasticmapreduce.model.JobFlowInstancesConfig;
import com.amazonaws.services.elasticmapreduce.model.RunJobFlowRequest;
import com.amazonaws.services.elasticmapreduce.model.RunJobFlowResult;
import com.amazonaws.services.elasticmapreduce.model.StepConfig;

/** 
 * This class is used as a Scheduler to run EMR JobFlow in periodic interval
 */
public class EMRDriver
{
    private static  String HADOOP_VERSION = "1.0.3";
    private static  int INSTANCE_COUNT = 1;
    private static  String INSTANCE_TYPE = InstanceType.M1Small.toString();
    private static  final UUID RANDOM_UUID = UUID.randomUUID();
    private static  String FLOW_NAME = "NG-Farmers-Portal-";
    private static  String BUCKET_NAME = "ngfarmer";
    private static  String S3N_HADOOP_JAR = "s3n://ngfarmer/job/ngfarmer.jar";
    private static  String S3N_LOG_URI = "s3n://" + BUCKET_NAME + "/logs";
    private static  String[] JOB_ARGS ;
    //		new String[] { "s3n://ngfarmer/input/",
    //                  	"s3n://ngfarmer/result"};

    //private static final List<String> ARGS_AS_LIST = Arrays.asList(JOB_ARGS);
    private static final List<JobFlowExecutionState> DONE_STATES = Arrays
        .asList(new JobFlowExecutionState[] { JobFlowExecutionState.COMPLETED,
                                             JobFlowExecutionState.FAILED,
                                             JobFlowExecutionState.TERMINATED });
    static AmazonElasticMapReduce emr;

    public EMRDriver() {
    }
    public EMRDriver(String bucketName) {
    	BUCKET_NAME = bucketName;
    	S3N_HADOOP_JAR = "s3n://"+bucketName+"/job/ngfarmer.jar";
    	S3N_LOG_URI = "s3n://" + bucketName + "/logs";
    	JOB_ARGS = new String[] { "s3n://"+bucketName+"/input/",
                          	"s3n://"+bucketName+"/result"};
    	System.out.println("Input params: \n\t\tBucketName:"+bucketName+" Hadoop Jar: "+S3N_HADOOP_JAR+
    						" \n\t\tLog URI: "+S3N_LOG_URI+" Job Args: "+JOB_ARGS[0]+" "+JOB_ARGS[1]);
    }
    /**
     * The only information needed to create a client are security credentials consisting of the AWS
     * Access Key ID and Secret Access Key. All other configuration, such as the service end points,
     * are performed automatically. Client parameters, such as proxies, can be specified in an
     * optional ClientConfiguration object when constructing a client.
     *
     * @see com.amazonaws.auth.BasicAWSCredentials
     * @see com.amazonaws.auth.PropertiesCredentials
     * @see com.amazonaws.ClientConfiguration
     */
    public int init() throws Exception {
    	AWSCredentials credentials = new ClasspathPropertiesFileCredentialsProvider().getCredentials();
        emr = new AmazonElasticMapReduceClient(credentials);
        Region usWest2 = Region.getRegion(Regions.US_WEST_2);
        emr.setRegion(usWest2);
        if (emr == null)
        	return 0;
        return 1;
    }
    public void setHadoopVersion(String version) {
    	HADOOP_VERSION = version;
    }
    public String getHadoopVersion() {
    	return HADOOP_VERSION;
    }
    public void setInstanceCount(int instance) {
    	INSTANCE_COUNT = instance;
    }
    public int getInstanceCount() {
    	return INSTANCE_COUNT;
    }
    public void setBucketName(String bucketname) {
    	BUCKET_NAME = bucketname;
    }
    public String getBucketName() {
    	return BUCKET_NAME;
    }
    
    public void runJobFlow() {
    	try 
    	{
            // Configure instances to use
            JobFlowInstancesConfig instances = new JobFlowInstancesConfig();
            System.out.println("Using EMR Hadoop v" + HADOOP_VERSION);
            instances.setHadoopVersion(HADOOP_VERSION);
            System.out.println("Using instance count: " + INSTANCE_COUNT);
            instances.setInstanceCount(INSTANCE_COUNT);
            System.out.println("Using master instance type: " + INSTANCE_TYPE);
            instances.setMasterInstanceType(INSTANCE_TYPE);
            System.out.println("Using slave instance type: " + INSTANCE_TYPE);
            instances.setSlaveInstanceType(INSTANCE_TYPE);

            // Configure the job flow
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm");
    		Calendar cal = Calendar.getInstance();
            String tmpFLOW_NAME = FLOW_NAME+dateFormat.format(cal.getTime());
            System.out.println("Configuring flow: " + tmpFLOW_NAME);
            RunJobFlowRequest request = new RunJobFlowRequest(tmpFLOW_NAME, instances);
            System.out.println("\tusing log URI: " + S3N_LOG_URI);
            request.setLogUri(S3N_LOG_URI);

            // Configure the Hadoop jar to use
            System.out.println("\tusing jar URI: " + S3N_HADOOP_JAR);
            HadoopJarStepConfig jarConfig = new HadoopJarStepConfig(S3N_HADOOP_JAR);
            
	 	    List<String> ARGS_AS_LIST = Arrays.asList(JOB_ARGS);
            System.out.println("\tusing args: " + ARGS_AS_LIST);

            jarConfig.setArgs(ARGS_AS_LIST);
            StepConfig stepConfig =
                new StepConfig(S3N_HADOOP_JAR.substring(S3N_HADOOP_JAR.indexOf('/') + 1),
                               jarConfig);
            request.setSteps(Arrays.asList(new StepConfig[] { stepConfig }));

            //Run the job flow
            RunJobFlowResult result = emr.runJobFlow(request);

            //Check the status of the running job
            String lastState = "";
            STATUS_LOOP: while (true)
            {
                DescribeJobFlowsRequest desc =
                    new DescribeJobFlowsRequest(
                                                Arrays.asList(new String[] { result.getJobFlowId() }));
                DescribeJobFlowsResult descResult = emr.describeJobFlows(desc);
                for (JobFlowDetail detail : descResult.getJobFlows())
                {
                    String state = detail.getExecutionStatusDetail().getState();
                    if (isDone(state))
                    {
                        System.out.println("Job " + state + ": " + detail.toString());
                        break STATUS_LOOP;
                    }
                    else if (!lastState.equals(state))
                    {
                        lastState = state;
                        System.out.println("Job " + state + " at " + new Date().toString());
                    }
                }
                // Sleep for 10 seconds before checking state of a Job Flow.
                Thread.sleep(10000);
            }
        } catch (AmazonServiceException ase) {
                System.out.println("Caught Exception: " + ase.getMessage());
                System.out.println("Reponse Status Code: " + ase.getStatusCode());
                System.out.println("Error Code: " + ase.getErrorCode());
                System.out.println("Request ID: " + ase.getRequestId());
        } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }
    public static void main(String[] args) throws Exception {

    	String bucketName = "";
    	if (args.length != 1) {
		      System.err.println("Usage: EMRDriver <bucketname>");
		      System.exit(2);
		}
        System.out.println("===========================================");
        System.out.println("Welcome to the Farmer's Point!!!");
        System.out.println("===========================================");
        
        /* S3 Initialization & backup old results, make sure result folder is deleted */
    	bucketName = args[0];
		String key = "result";

		S3Service svc = new S3Service(bucketName);
		svc.transferFromRDStoS3();
		svc.backupResult(bucketName, key);
		
	 	EMRDriver emr = new EMRDriver(bucketName);

		/* Initialize Elastic MapReduce*/
        int result = emr.init();
        if (result == 0) {
            System.out.println("EMR Instance creation failed...");
        }
        
        /*Create and run a EMR Cluster/JobFlow */
        emr.runJobFlow();
		svc.transferFromS3toDynoDb(bucketName, key+"/part-r-00000");

        System.out.println("EMR Cluster Job Done. Terminated...");
    }

    /**
     * @param state
     * @return
     */
    public static boolean isDone(String value)
    {
        JobFlowExecutionState state = JobFlowExecutionState.fromValue(value);
        return DONE_STATES.contains(state);
    }
}